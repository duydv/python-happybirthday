﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pymysql.cursors
import random
from datetime import datetime, date
import time
from slackclient import SlackClient

class SlackService:
    def __init__(self,namebot,token):
        self.Name = namebot
        self.token = token
        self.client = SlackClient(self.token)
    def SendMessage(self,chanel,title,titlesub,mes,linkimg):
        att = [{"pretext":title,
                            "fallback": "Required plain-text summary of the attachment.",
                            "color": "#36a64f",
                            "author_icon": "http://www.freeiconspng.com/uploads/simpson-happy-icon-19.png",
                            "title": titlesub,
                            "title_link": "https://api.slack.com/",
                            "text":  mes,
                            "image_url": linkimg
                           }]
        self.client.api_call("chat.postMessage", username=self.Name, channel=chanel,attachments=att,link_names =1)

class AppService:
    def __init__(self):
        self.connection = pymysql.connect(host='192.168.1.2',
                                 user='Quyetqv',
                                 password='Quyetqv',
                                 db='dbbirthday',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
        
    def GetAllLinkImg(self,type):
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM tb_image WHERE Type = %s"
                cursor.execute(sql,(type,))
                imglink = cursor.fetchall()
                print(imglink)
                return imglink
        except  ValueError:
            print ValueError
            return []
    def GetAllWish(self,type):
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM tb_wish WHERE Type = %s"
                cursor.execute(sql,(type,))
                imglink = cursor.fetchall()
                print(imglink)
                return imglink
        except  ValueError:
            print ValueError
            return []
    def close(self):
        self.connection.close()
    def GetEmployeeBydate(self,month,day):
        try:
            with self.connection.cursor() as cursor:
                sql = "SELECT * FROM tb_Employee WHERE MONTH(Birthday) = %s and DAY(Birthday) = %s"
                cursor.execute(sql,(str(month),str(day),))
                result = cursor.fetchall()
                print(result)
                return result
        except  ValueError:
            print ValueError
            return []
   
mytype = ("birthday","chamngon")
nv = AppService()
today = datetime.now()

#lay danh sach nhan vien co sinh nhat vao ngay hien tai
dsnhanvien = nv.GetEmployeeBydate(today.month,today.day)

#dinh nghia cac gia tri cho slackbot
namebot = "Happy"    
api_key = 'xoxb-68420109287-Zw6V0P0vOQaqtevUHF2rYF8T'
#api_key = 'xoxb-66134404759-jzCZwMq1llmlLaTQDn9VfJWl'
chandev = "#chemgio"
client = SlackService(namebot,api_key)

print("______**********______ \n")
if dsnhanvien:
    print "chuc mung tat ca nhan vien co sinh nhat"
    for x in dsnhanvien:
        dataimg = nv.GetAllLinkImg(mytype[0])
        datawish = nv.GetAllWish(mytype[0])
        noidung = ""
        linkimg = ""
        tieude =""
        tieudesub ="Happy birthday to you"
        if dataimg:
            i = random.choice(dataimg)
            linkimg = i["Url_Img"]
        if datawish:
            dw = random.choice(datawish)
            noidung = dw["Content"].encode('utf8')
        tieude = "Hôm nay là một ngày đặc biệt của " + x["Name"].encode("utf8") + " :grinning::grinning:"
        client.SendMessage(chandev,tieude,tieudesub,noidung,linkimg)

else:
    print("chao mot ngay moi")
    dataimg = nv.GetAllLinkImg(mytype[1])
    datawish = nv.GetAllWish(mytype[1])
    noidung = ""
    linkimg = ""
    tieude =""
    tieudesub ="Chào buổi sáng"
    if dataimg:
        i = random.choice(dataimg)
        linkimg = i["Url_Img"]
    if datawish:
        dw = random.choice(datawish)
        noidung = dw["Content"].encode('utf8')
    client.SendMessage(chandev,tieude,tieudesub,noidung,linkimg)
    
nv.close()



